<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAuthors extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('authors')->insert(array(
			'name'=>'Nasa',
			'bio' => 'Nasa is great programmer',
			'created_at' => date('Y-m-d H:m:s'),
			'updated_at' => date('Y-m-d H:m:s')
		));

		DB::table('authors')->insert(array(
			'name'=>'author2',
			'bio' => 'author is great programmer',
			'created_at' => date('Y-m-d H:m:s'),
			'updated_at' => date('Y-m-d H:m:s')
		));

		DB::table('authors')->insert(array(
			'name'=>'author3',
			'bio' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. In, quam.',
			'created_at' => date('Y-m-d H:m:s'),
			'updated_at' => date('Y-m-d H:m:s')
		));

		DB::table('authors')->insert(array(
			'name'=>'author4',
			'bio' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. In, quam.',
			'created_at' => date('Y-m-d H:m:s'),
			'updated_at' => date('Y-m-d H:m:s')
		));

		DB::table('authors')->insert(array(
			'name'=>'author5',
			'bio' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. In, quam.',
			'created_at' => date('Y-m-d H:m:s'),
			'updated_at' => date('Y-m-d H:m:s')
		));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('authors')->where('name', '=', 'Nasa')->delete();
		DB::table('authors')->where('name', '=', 'author2')->delete();
	}

}
