<?php

class AuthorsController extends BaseController
{
    public $restful = true;


    public function index() {
        return View::make('authors.index')
            ->with('title','Authors And Books')
            ->with('authors', Author::orderBy('name')->get());
    }

    public function view($id) {
        return View::make('authors.view')
          ->with('title', 'Author view Page')
          ->with('author', Author::find($id));
    }

    public function new_author() {
        return View::make('authors.new')
        ->with('title', 'New Author');
    }

    public function create() {



        if($validator->fails()) {
            return Redirect::to('new_author')->with_errors($validation)->with_input();
        } else {
            Author::create(array(
                'name'=>Input::get('name'),
                'bio'=>Input::get('bio'),
                'created_at'=>date('Y-m-d H:m:s')
            ));

            return Redirect::to('authors')->with('message', 'The author was created');
        }
    }
}
