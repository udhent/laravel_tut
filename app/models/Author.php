<?php

class Author extends Eloquent {
    public $table = 'authors';
    public static $unguarded = true;

   $validator = Validator::make(
        array('name' => 'required|min:2',
            'bio' => 'required|min:10')
        );
}
